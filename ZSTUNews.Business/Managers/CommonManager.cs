﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZSTUNews.Business.Entities;
using ZSTUNews.Business.ProviderInterfaces;
using ZSTUNews.Business.UnitOfWorkInterfaces;

namespace ZSTUNews.Business.Managers
{
    public class CommonManager : Manager<User, IUserProvider>
    {
        private readonly IArticleProvider articleProvider;

        public CommonManager(IUserProvider provider, IUnitOfWorkFactory unitOfWorkFactory, IArticleProvider articleProvider)
            : base(provider, unitOfWorkFactory)
        {
            this.articleProvider = articleProvider;
        }

        public Task<Article> GetArticleById(string articleId)
        {
            if (string.IsNullOrEmpty(articleId))
                throw new ArgumentException(nameof(articleId));

            return articleProvider.Get(articleId);
        }

        public async Task CreateUser(User user)
        {
            if (user == null)
                throw new ArgumentNullException(nameof(user));

            user.Id = Guid.NewGuid().ToString();

            using (var uow = unitOfWorkFactory.Create())
            {
                uow.BeginTransaction();
                await provider.SaveOrUpdate(user);
                await uow.Commit();
            }
        }

        public async Task CreateArticle(Article article)
        {
            if (article == null)
                throw new ArgumentNullException(nameof(article));

            article.Id = Guid.NewGuid().ToString();

            using (var uow = unitOfWorkFactory.Create())
            {
                uow.BeginTransaction();
                await articleProvider.SaveOrUpdate(article);
                await uow.Commit();
            }
        }

        public async Task UpdateUser(User user)
        {
            if (user == null)
                throw new ArgumentNullException(nameof(user));

            if (string.IsNullOrEmpty(user.Id))
                throw new ArgumentException(nameof(user.Id));

            using (var uow = unitOfWorkFactory.Create())
            {
                uow.BeginTransaction();
                await provider.SaveOrUpdate(user);
                await uow.Commit();
            }
        }

        public async Task UpdateArticle(Article article)
        {
            if (article == null)
                throw new ArgumentNullException(nameof(article));

            if (string.IsNullOrEmpty(article.Id))
                throw new ArgumentException(nameof(article.Id));

            using (var uow = unitOfWorkFactory.Create())
            {
                uow.BeginTransaction();
                await articleProvider.SaveOrUpdate(article);
                await uow.Commit();
            }
        }

        public async Task DeleteUser(User user)
        {
            if (user == null)
                throw new ArgumentNullException(nameof(user));
            
            using (var uow = unitOfWorkFactory.Create())
            {
                uow.BeginTransaction();
                await provider.Delete(user);
                await uow.Commit();
            }
        }

        public async Task DeleteArticle(Article article)
        {
            if (article == null)
                throw new ArgumentNullException(nameof(article));

            using (var uow = unitOfWorkFactory.Create())
            {
                uow.BeginTransaction();
                await articleProvider.Delete(article);
                await uow.Commit();
            }
        }

        public async Task DeleteUser(string userId)
        {
            if (string.IsNullOrEmpty(userId))
                throw new ArgumentException(nameof(userId));

            using (var uow = unitOfWorkFactory.Create())
            {
                uow.BeginTransaction();
                await provider.Delete(userId);
                await uow.Commit();
            }
        }

        public Task<User> GetUserByUserName(string userName)
        {
            if (string.IsNullOrEmpty(userName))
                throw new ArgumentException(nameof(userName));

            return provider.GetUserByUserName(userName);
        }

        public Task<IList<Article>> GetPageArticles(int pageIndex, int articlesQuantity, bool sortedByDate)
        {
            return articleProvider.GetPageArticles(pageIndex, articlesQuantity, sortedByDate);
        }

        public Task<int> GetArticlesQuantity()
        {
            return articleProvider.GetArticlesQuantity();
        }

        public Task<IList<User>> GetUsersList()
        {
            return provider.GetUsersList();
        }
    }
}
