﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZSTUNews.Business.UnitOfWorkInterfaces
{
    public interface IUnitOfWork : IDisposable
    {
        void BeginTransaction();
        Task Commit();
        Task Rollback();
    }
}
