﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZSTUNews.Business.Entities;

namespace ZSTUNews.Business.ProviderInterfaces
{
    public interface IArticleProvider : IProvider<Article>
    {
        Task<int> GetArticlesQuantity();
        Task<IList<Article>> GetPageArticles(int pageIndex, int articlesQuantity, bool sortedByDate);
    }
}
