﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ZSTUNews.Business.Entities;

namespace ZSTUNews.Business.ProviderInterfaces
{
    public interface IUserProvider : IProvider<User>
    {
        Task<User> GetUserByUserName(string userName);
        Task<IList<User>> GetUsersList();
    }
}
