﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZSTUNews.Business.Entities;

namespace ZSTUNews.Business.ProviderInterfaces
{
    public interface IProvider<T>
        where T : IEntity
    {
        Task<T> Get(string id);
        Task<IList<T>> GetByIdList(IList<string> ids);
        Task<T> SaveOrUpdate(T entity);
        Task Delete(string id);
        Task Delete(T entity);
    }
}
