﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZSTUNews.Business.Entities
{
    public class User : IEntity
    {
        public virtual string Id { get; set; }

        public virtual string UserName { get; set; }

        public virtual string Role { get; set; }

        public virtual string Email { get; set; }

        public virtual string PasswordHash { get; set; }

        public virtual string PhoneNumber { get; set; }
    }
}
