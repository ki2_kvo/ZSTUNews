﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZSTUNews.Business.Entities
{
    public class Article : IEntity
    {
        public virtual string Id { get; set; }
        public virtual string UserId { set; get; }
        public virtual string Title { set; get; }
        public virtual string Content { set; get; }
        public virtual DateTime CreationDate { set; get; }
    }
}
