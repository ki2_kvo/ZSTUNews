﻿using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZSTUNews.Business.Entities;

namespace ZSTUNews.Nhibernate.Mappings
{
    public class UserMap : ClassMap<User>
    {
        public UserMap()
        {
            Table("Users");
            Id(x => x.Id).GeneratedBy.Assigned();
            Map(x => x.UserName).Not.Nullable().Unique();
            Map(x => x.Role).Not.Nullable();
            Map(x => x.Email).Not.Nullable().Unique();
            Map(x => x.PasswordHash).Not.Nullable();
            Map(x => x.PhoneNumber);
        }
    }
}
