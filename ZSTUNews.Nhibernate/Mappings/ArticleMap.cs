﻿using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZSTUNews.Business.Entities;

namespace ZSTUNews.Nhibernate.Mappings
{
    public class ArticleMap : ClassMap<Article>
    {
        public ArticleMap()
        {
            Table("Articles");
            Id(x => x.Id).GeneratedBy.Assigned();
            Map(x => x.UserId).Not.Nullable();
            Map(x => x.Title);
            Map(x => x.Content);
            Map(x => x.CreationDate);
        }
    }
}
