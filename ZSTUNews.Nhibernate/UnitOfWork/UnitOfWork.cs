﻿using NHibernate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using ZSTUNews.Business;
using ZSTUNews.Business.UnitOfWorkInterfaces;

namespace ZSTUNews.Nhibernate.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        private const string CurrentSessionKey = "CurrentSessionKey";

        private readonly ISession session;
        private ITransaction transaction;

        public UnitOfWork(ISession session)
        {
            this.session = session;
        }

        public void BeginTransaction()
        {
            if(transaction == null || !transaction.IsActive)
                transaction = session.BeginTransaction();
        }

        public async Task Commit()
        {
            try
            {
                await transaction.CommitAsync();
            }
            catch
            {
                await Rollback();
                throw;
            }
            finally
            {
                Dispose();
            }
        }

        public async Task Rollback()
        {
            try
            {
                await transaction.RollbackAsync();
            }
            finally
            {
                Dispose();
            }
        }

        public void Dispose()
        {
            transaction.Dispose();
        }
    }
}
