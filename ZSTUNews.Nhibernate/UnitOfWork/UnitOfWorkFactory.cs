﻿using NHibernate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZSTUNews.Business;
using ZSTUNews.Business.UnitOfWorkInterfaces;

namespace ZSTUNews.Nhibernate.UnitOfWork
{
    public class UnitOfWorkFactory : IUnitOfWorkFactory
    {
        private readonly IDataStorage dataStorage;

        public UnitOfWorkFactory(IDataStorage dataStorage)
        {
            this.dataStorage = dataStorage;
        }

        public IUnitOfWork Create()
        {
            var session = dataStorage.GetValue<ISession>("CurrentSessionKey");
            if (session == null)
            {
                session = Configuration.SessionFactory.OpenSession();
                dataStorage.SetValue("CurrentSessionKey", session);
            }

            return new UnitOfWork(session);
        }
    }
}
