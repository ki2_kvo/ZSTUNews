﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZSTUNews.Business;
using ZSTUNews.Business.Entities;
using ZSTUNews.Business.ProviderInterfaces;
using ZSTUNews.Business.UnitOfWorkInterfaces;

namespace ZSTUNews.Nhibernate.Providers
{
    public abstract class Provider<T> : IProvider<T>
        where T : class, IEntity
    {
        protected readonly ProviderHelper providerHelper;

        public Provider(IDataStorage dataStorage)
        {
            providerHelper = new ProviderHelper(dataStorage);
        }

        public Task<T> Get(string id)
        {
            if (string.IsNullOrEmpty(id))
                throw new ArgumentNullException(nameof(id));

            return providerHelper.Invoke(s => s.GetAsync<T>(id));
        }

        public Task<IList<T>> GetByIdList(IList<string> ids)
        {
            if (ids == null)
                throw new ArgumentNullException(nameof(ids));

            if (ids.Count == 0)
                return Task.FromResult<IList<T>>(new List<T>());

            return providerHelper.Invoke(s => s.QueryOver<T>()
            .WhereRestrictionOn(it => it.Id)
            .IsIn(new Collection<string>(ids))
            .ListAsync());
        }

        public async Task<T> SaveOrUpdate(T entity)
        {
            if (entity == null)
                throw new ArgumentNullException(nameof(entity));

            await providerHelper.Invoke(s => s.SaveOrUpdateAsync(entity));

            return entity;

        }

        public async Task Delete(T entity)
        {
            if (entity == null)
                throw new ArgumentNullException(nameof(entity));

            if (string.IsNullOrEmpty(entity.Id))
                throw new ArgumentException(nameof(entity) + nameof(entity.Id));

            await providerHelper.Invoke(s => s.DeleteAsync(entity));
        }

        public async Task Delete(string id)
        {
            if (string.IsNullOrEmpty(id))
                throw new ArgumentNullException(nameof(id));

            await Delete(await Get(id));
        }
    }
}
