﻿using NHibernate.Criterion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZSTUNews.Business;
using ZSTUNews.Business.Entities;
using ZSTUNews.Business.ProviderInterfaces;

namespace ZSTUNews.Nhibernate.Providers
{
    public class ArticleProvider : Provider<Article>, IArticleProvider
    {
        public ArticleProvider(IDataStorage dataStorage) : base(dataStorage)
        {
        }

        public Task<int> GetArticlesQuantity()
        {
            return providerHelper.Invoke(s => s.QueryOver<Article>().RowCountAsync());
        }

        public Task<IList<Article>> GetPageArticles(int pageIndex, int articlesQuantity, bool sortedByDate)
        {
            var propertyForOrder = "CreationDate";
            var orderType = sortedByDate ? Order.Desc(propertyForOrder) : Order.Asc(propertyForOrder);
            return providerHelper.Invoke(s => s.CreateCriteria<Article>().
            AddOrder(orderType).
            SetFirstResult(pageIndex * articlesQuantity).
            SetMaxResults(articlesQuantity).
            ListAsync<Article>());
        }

        public Task<User> GetUserByUserName(string userName)
        {
            return providerHelper.Invoke(s => s.QueryOver<User>().Where(it => it.UserName == userName).SingleOrDefaultAsync());
        }
    }
}
