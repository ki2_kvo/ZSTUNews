﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ZSTUNews.Business;
using ZSTUNews.Business.Entities;
using ZSTUNews.Business.ProviderInterfaces;

namespace ZSTUNews.Nhibernate.Providers
{
    public class UserProvider : Provider<User>, IUserProvider
    {
        public UserProvider(IDataStorage dataStorage) : base(dataStorage)
        {
        }

        public Task<User> GetUserByUserName(string userName)
        {
            return providerHelper.Invoke(s => s.QueryOver<User>().Where(it => it.UserName == userName).SingleOrDefaultAsync());
        }

        public Task<IList<User>> GetUsersList()
        {
            return providerHelper.Invoke(s => s.QueryOver<User>().ListAsync());
        }
    }
}
