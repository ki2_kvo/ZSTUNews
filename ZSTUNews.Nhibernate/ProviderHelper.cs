﻿using NHibernate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZSTUNews.Business;

namespace ZSTUNews.Nhibernate
{
    public class ProviderHelper
    {
        private readonly IDataStorage dataStorage;

        public ProviderHelper(IDataStorage dataStorage)
        {
            this.dataStorage = dataStorage;
        }

        public async Task<TResult> Invoke<TResult>(Func<ISession, Task<TResult>> databaseAction)
        {
            var session = dataStorage.GetValue<ISession>("CurrentSessionKey");
            if (session == null)
            {
                session = Configuration.SessionFactory.OpenSession();
                dataStorage.SetValue("CurrentSessionKey", session);
            }

            return await databaseAction(session);
        }

        public async Task Invoke(Func<ISession, Task> databaseAction)
        {
            await Invoke<object>(async s =>
            {
                await databaseAction(s);
                return Task.CompletedTask;
            });
        }
    }
}
