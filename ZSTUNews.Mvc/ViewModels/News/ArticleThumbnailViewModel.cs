﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ZSTUNews.Mvc.ViewModels.News
{
    public class ArticleThumbnailViewModel
    {
        public string Id { set; get; }
        public string UserId { set; get; }
        public string UserName { set; get; }
        public string Title { set; get; }

        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy HH:mm}", ApplyFormatInEditMode = true)]
        public DateTime CreationDate { set; get; }
    }
}
