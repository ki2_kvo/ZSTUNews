﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZSTUNews.Mvc.ViewModels.News
{
    public class NewsPageViewModel
    {
        public IEnumerable<ArticleThumbnailViewModel> Thumbnails { set; get; }
        public int CurrentPageIndex { set; get; }
        public int LastPageIndex { set; get; }
        public bool SortedByDate { set; get; }
    }
}
