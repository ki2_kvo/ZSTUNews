﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ZSTUNews.Mvc.ViewModels.News
{
    public class DialogViewModel
    {
        public string Id { set; get; }
        public string Title { set; get; }

        public bool Agreement { set; get; }
    }
}