﻿using System;
using System.Collections.Generic;

namespace ZSTUNews.Mvc.ViewModels.News
{
    public class ArticleMainPageViewModel
    {
        public string Id { set; get; }
        public string Title { set; get; }
        public string Content { set; get; }
        public DateTime CreationDate { set; get; }
        public string UserId { set; get; }
        public string UserName { set; get; }
    }
}