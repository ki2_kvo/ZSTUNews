﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using ZSTUNews.Business.Entities;
using ZSTUNews.Business.Managers;
using ZSTUNews.Mvc.ViewModels.News;

namespace ZSTUNews.Mvc.Controllers
{
    public class NewsController : Controller
    {
        private readonly CommonManager commonManager;

        public NewsController(CommonManager commonManager)
        {
            this.commonManager = commonManager;
        }

        public async Task<ActionResult> Index(int page = 0, bool sortedByDate = true)
        {
            int articlesQuantityPerPage = int.Parse(ConfigurationManager.AppSettings["articleQuantityPerPage"]);
            
            int lastPage = (int)Math.Ceiling(await commonManager.GetArticlesQuantity() / (double)articlesQuantityPerPage) - 1;
            if (lastPage == -1)
                lastPage = 0;
            else if (page < 0 || page > lastPage)
                return HttpNotFound();

            var articleList = await commonManager.GetPageArticles(page, articlesQuantityPerPage, sortedByDate);
            var thumbnails = new List<ArticleThumbnailViewModel>();

            foreach (var article in articleList)
            {
                var user = await commonManager.GetById(article.UserId);
                var userName = user?.UserName ?? string.Empty;
                thumbnails.Add(new ArticleThumbnailViewModel()
                {
                    Id = article.Id,
                    Title = article.Title,
                    UserId = article.UserId,
                    CreationDate = article.CreationDate,
                    UserName = userName
                });
            }

            var newsPageModel = new NewsPageViewModel()
            {
                Thumbnails = thumbnails,
                CurrentPageIndex = page,
                LastPageIndex = lastPage,
                SortedByDate = sortedByDate,

            };

            return View(newsPageModel);
        }

        [Authorize]
        public ActionResult Add()
        {
            return View();
        }

        [Authorize]
        [HttpPost]
        [ValidateInput(false)]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Add(ArticleAddViewModel newsModel)
        {
            if (!ModelState.IsValid)
                return View(newsModel);

            var article = new Article()
            {
                UserId = User.Identity.GetUserId(),
                Title = newsModel.Title,
                Content = newsModel.Content,
                CreationDate = DateTime.Now
            };

            await commonManager.CreateArticle(article);
            return RedirectToAction("Index", "News");
        }

        [Authorize]
        public async Task<ActionResult> Edit(string articleId)
        {
            if (articleId == null)
                return HttpNotFound();

            var article = await commonManager.GetArticleById(articleId);
            if (article == null)
                return HttpNotFound();

            bool isUserArticleOwner = article.UserId == User.Identity.GetUserId();
            if (!isUserArticleOwner)
            {
                return View("NewsOwnerError");
            }

            var articleForEditing = new ArticleViewModel()
            {
                Id = article.Id,
                UserId = article.UserId,
                Title = article.Title,
                Content = article.Content,
                CreationDate = article.CreationDate
            };

            return View(articleForEditing);
        }

        [HttpPost]
        [Authorize]
        [ValidateInput(false)]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(ArticleViewModel editModel)
        {
            if (!ModelState.IsValid)
                return View(editModel);

            var editedArticle = new Article()
            {
                Id = editModel.Id,
                UserId = editModel.UserId,
                Title = editModel.Title,
                Content = editModel.Content,
                CreationDate = editModel.CreationDate
            };

            await commonManager.UpdateArticle(editedArticle);

            return RedirectToAction("Index", "News");
        }

        public async Task<ActionResult> Article(string articleId)
        {
            if (articleId == null)
                return HttpNotFound();

            var article = await commonManager.GetArticleById(articleId);
            if (article == null)
                return HttpNotFound();

            var newsUser = await commonManager.GetById(article.UserId);

            var showMainNews = new ArticleMainPageViewModel()
            {
                Id = article.Id,
                Title = article.Title,
                Content = article.Content,
                CreationDate = article.CreationDate,
                UserId = article.UserId,
                UserName = newsUser.UserName
            };

            return View(showMainNews);
        }

        [HttpPost]
        [Authorize]
        public async Task<ActionResult> Delete(string articleId)
        {
            if (articleId == null)
                return HttpNotFound();

            var article = await commonManager.GetArticleById(articleId);
            if (article == null)
                return HttpNotFound();

            await commonManager.DeleteArticle(article);

            return RedirectToAction("Index", "News");
        }
    }
}
