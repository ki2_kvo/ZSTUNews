﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using ZSTUNews.Business.Entities;
using ZSTUNews.Business.Managers;
using ZSTUNews.Nhibernate;

namespace ZSTUNews.Mvc.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}