﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace ZSTUNews.Mvc.Helpers
{
    public static class TitleReplacer
    {
        public static string EditNewsTitleForUrl(this HtmlHelper html, string title)
        {
            string titleForEdit = Regex.Replace(title, @"(^\W+|\W+$)", "").Trim();
            return Regex.Replace(titleForEdit, @"\W+", "-");
        }
    }
}
