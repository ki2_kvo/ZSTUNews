﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(ZSTUNews.Web.Startup))]
namespace ZSTUNews.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
