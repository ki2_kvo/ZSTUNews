﻿using System.Web;
using System.Web.Optimization;

namespace ZSTUNews.Web
{
    public class BundleConfig
    {
        // Дополнительные сведения об объединении см. на странице https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new StyleBundle("~/bundles/stylesheet-Site").Include("~/Content/Site.css"));
            bundles.Add(new StyleBundle("~/bundles/stylesheet-Basic").Include("~/Content/StyleSheet_BasicFormatting.css"));
            bundles.Add(new StyleBundle("~/bundles/stylesheet-Help").Include("~/Content/StyleSheet_HelpStyles.css"));
            bundles.Add(new StyleBundle("~/bundles/stylesheet-Home").Include("~/Content/StyleSheet_HomePage.css"));
            bundles.Add(new StyleBundle("~/bundles/stylesheet-News").Include("~/Content/StyleSheet_NewsPage.css"));
            bundles.Add(new StyleBundle("~/bundles/stylesheet-Service").Include("~/Content/StyleSheet_ServicePages.css"));
            bundles.Add(new StyleBundle("~/bundles/stylesheet-Start").Include("~/Content/StyleSheet_StartPage.css"));

            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Используйте версию Modernizr для разработчиков, чтобы учиться работать. Когда вы будете готовы перейти к работе,
            // готово к выпуску, используйте средство сборки по адресу https://modernizr.com, чтобы выбрать только необходимые тесты.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css"));
        }
    }
}
