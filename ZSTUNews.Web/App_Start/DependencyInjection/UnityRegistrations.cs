﻿using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Unity;
using Unity.Injection;
using ZSTUNews.Business;
using ZSTUNews.Business.ProviderInterfaces;
using ZSTUNews.Business.UnitOfWorkInterfaces;
using ZSTUNews.Mvc.Identity;
using ZSTUNews.Mvc.Identity.Managers;
using ZSTUNews.Mvc.Identity.Models;
using ZSTUNews.Nhibernate.Providers;
using ZSTUNews.Nhibernate.UnitOfWork;

namespace ZSTUNews.Web.App_Start.DependencyInjection
{
    public static class UnityRegistrations
    {
        public static void RegisterTypes(IUnityContainer container)
        {
            container.RegisterType<IUserProvider, UserProvider>();
            container.RegisterType<IArticleProvider, ArticleProvider>();

            container.RegisterType<ApplicationUserManager, ApplicationUserManager>();
            container.RegisterType<IUserStore<ApplicationUser, string>, IdentityUserStore>();
            container.RegisterType<IAuthenticationManager, IAuthenticationManager>(
                new InjectionFactory(c => HttpContext.Current.GetOwinContext().Authentication));

            container.RegisterType<IDataStorage, RequestDataStorage>();
            container.RegisterType<IUnitOfWorkFactory, UnitOfWorkFactory>();
        }
    }
}